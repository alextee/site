#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'David Robillard'
SITENAME = u'LV2'
SITELOGO = 'images/logo.svg'
SITELOGO_WIDTH =  '26.194574em'
SITELOGO_HEIGHT = '24.525000em'
SITEURL = 'http://lv2plug.in'

PATH = 'content'

TIMEZONE = 'America/Toronto'

DEFAULT_LANG = u'en'
ARTICLE_TRANSLATION_ID = None
PAGE_TRANSLATION_ID = None

THEME = 'themes/lv2'

FAVICON = 'images/favicon.png'

# Blogroll
LINKS = ()

# Social widget
SOCIAL = ()

MENUITEMS = [('News', '/news.html'),
             ('Reference', 'http://lv2plug.in/ns/'),
             ('Developing', '/pages/developing.html'),
             ('Git', 'http://gitlab.com/lv2/lv2')]

DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False

DEFAULT_PAGINATION = False
SUMMARY_MAX_LENGTH = None

DIRECT_TEMPLATES = ['news']
PAGINATED_DIRECT_TEMPLATES = ['news']
ARTICLE_PATHS = ['news']
PAGE_URL = 'pages/{slug}'
PAGE_PATHS = ['pages']

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True


DISPLAY_TAGS_ON_SIDEBAR = False
DISPLAY_RECENT_POSTS_ON_SIDEBAR = False
DISPLAY_SERIES_ON_SIDEBAR = False
DISPLAY_CATEGORIES_ON_SIDEBAR = False

PLUGIN_PATHS = ["extra_plugins"]
PLUGINS = ["gallery"]

GALLERY_FOLDER = "galleries"
GALLERY_SRC_PATH = "%s%s" % (PATH, "/images/screenshots")
GALLERY_OUTPUT_PATH = "%s%s" % ("output/", GALLERY_FOLDER)
GALLERY_REGENERATE_EXISTING = True
GALLERY_PRESETS = [
    {"name": "thumb",
     "actions": [{"type": "fit", "height": 100, "width": 100, "from": (0.5, 0.5)}]},
    {"name": "slider",
     "actions" : []},
    {"name": "large",
     "actions": [{"type": "resize", "height": 640, "width": 850, "from": (0.5, 0.5)}]},
    {"name": "thumb_greyscale",
     "actions": [{"type": "fit", "height": 100, "width": 100, "from": (0.5, 0.5)},
                 {"type": "greyscale"} ]}]
