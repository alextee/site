Title:
save_as: index.html
Gallery: .

LV2 is an open extensible standard for audio plugins.
LV2 has a simple core interface,
which is accompanied by extensions that add more advanced functionality.

Many types of plugins can be built with LV2, including audio effects, synthesizers, and control processors for modulation and automation.
Extensions support more powerful features, such as:

 * Platform-native UIs
 * Network-transparent plugin control
 * Portable/archivable persistent state
 * Threaded tasks (like file loading) with sample-accurate export
 * Deep semantic plugin discovery and control with meaningful units

The LV2 specification itself as well as the accompanying libraries are permissively licensed free software, with support for all major platforms.

<a class="biglink" href="http://lv2plug.in/spec/lv2-1.16.0.tar.bz2">Download LV2 1.16.0</a>
<a class="siglink" href="http://lv2plug.in/spec/lv2-1.16.0.tar.bz2.asc">sig</a>

Learn
-----

* [Why LV2?](pages/why-lv2.html)
* [Developing with LV2](pages/developing.html)

Discover
--------

* [Projects using LV2](pages/projects.html)
* [Mailing list](http://lists.lv2plug.in/listinfo.cgi/devel-lv2plug.in)
* [Chat](http://webchat.freenode.net/?channels=lv2)
