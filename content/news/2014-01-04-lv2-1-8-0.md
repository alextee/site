Title: lv2 1.8.0
Date: 2014-01-04 00:00
Slug: lv2-1-8-0
Author: drobilla

[lv2 1.8.0](http://lv2plug.in/spec/lv2-1.8.0.tar.bz2) has been released.

Changes:

 * Add scope example plugin from Robin Gareus.
 * Install lv2specgen for use by other projects.
 * lv2specgen: Fix links to externally defined terms.
 * atom: Make lv2_atom_*_is_end() arguments const.
 * core: Add lv2:prototype for property inheritance.
 * log: Add missing include string.h to logger.h for memset.
 * ui: Fix LV2_UI_INVALID_PORT_INDEX identifier in documentation.
